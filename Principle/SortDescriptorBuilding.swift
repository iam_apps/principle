//
//  SortDescriptorBuilding.swift
//  Predikit
//
//  Created by Alex Lynch on 3/24/18.
//  Copyright © 2018 IAM Apps. All rights reserved.
//

import Foundation
#if canImport(ObjectiveC)

public class SortDescriptor<S> : NSSortDescriptor {
	
	fileprivate init(from sortDescriptor: NSSortDescriptor) {
		super.init(key: sortDescriptor.key, ascending: sortDescriptor.ascending)
	}
	
	override public init(key: String?, ascending: Bool, selector: Selector?) {
		super.init(key: key, ascending: ascending, selector: selector)
	}
	
	required public init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

extension KeyPath where Value : Comparable {
	public subscript(comparison: GeneralComparison) -> SortDescriptor<Root> {
		switch comparison {
		case .asc:
			return SortDescriptor<Root>(from: NSSortDescriptor(keyPath: self, ascending: true))
		case .desc:
			return SortDescriptor<Root>(from: NSSortDescriptor(keyPath: self, ascending: false))
		}
	}
}

extension KeyPath where Value : OptionalProtocol, Value.WrappedType : Comparable {
	public subscript(comparison: GeneralComparison) -> SortDescriptor<Root> {
		switch comparison {
		case .asc:
			return SortDescriptor<Root>(from: NSSortDescriptor(keyPath: self, ascending: true))
		case .desc:
			return SortDescriptor<Root>(from: NSSortDescriptor(keyPath: self, ascending: false))
		}
	}
}

public func | <Root,Value>(lhs: KeyPath<Root,Value>, rhs: GeneralComparison) -> SortDescriptor<Root>  where Value : OptionalProtocol, Value.WrappedType : Comparable {
	return lhs[rhs]
}

public func | <Root,Value>(lhs: KeyPath<Root,Value>, rhs: GeneralComparison) -> SortDescriptor<Root>  where Value : Comparable {
	return lhs[rhs]
}

public func | <Root,Value>(lhs: KeyPath<Root,Value>, rhs: DateComparison) -> SortDescriptor<Root>  where Value : OptionalProtocol, Value.WrappedType == Date {
	return lhs[rhs.genaralComparison]
}

public func | <Root>(lhs: KeyPath<Root,Date>, rhs: DateComparison) -> SortDescriptor<Root> {
	return lhs[rhs.genaralComparison]
}

#endif 
