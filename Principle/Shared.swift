//
//  Shared.swift
//  Predikit
//
//  Created by Alex Lynch on 3/25/18.
//  Copyright © 2018 IAM Apps. All rights reserved.
//

import Foundation

/// Protocol for handling Optionals as a protocol. Useful for generic constraints
public protocol OptionalProtocol {
	/// The type which this optional wraps
	associatedtype WrappedType
	
	/// Return an optional value of the wrapped type.
	///
	/// - Returns: an optional value
	var wrapped: WrappedType? { get }
}

/// Optional implements OptionalProtocol for use in generic constraints
extension Optional : OptionalProtocol {
	/// The type which this optional wraps
	public typealias WrappedType = Wrapped
	
	/// return self
	public var wrapped: WrappedType? {
		return self
	}
}

public enum GeneralComparison {
	case asc
	case desc
}

public let asc = GeneralComparison.asc
public let desc = GeneralComparison.desc

protocol GeneralComparisonEquivalent {
	var genaralComparison : GeneralComparison { get }
}

public enum DateComparison : GeneralComparisonEquivalent {
	/// same as asc
	case oldToNew
	/// same as desc
	case newToOld
	
	var genaralComparison: GeneralComparison {
		switch self {
			case .oldToNew: return .asc
			case .newToOld: return .desc
		}
	}
}

public let oldToNew = DateComparison.oldToNew
public let newToOld = DateComparison.newToOld


infix operator | : AdditionPrecedence

public enum StringOperator {
	public struct Options : OptionSet {
		public var rawValue: Int
		
		public init(rawValue: Int) {
			self.rawValue = rawValue
		}
		
		public static let caseInsensitive       = Options(rawValue: 0b01)
		public static let diacriticInsensitive  = Options(rawValue: 0b10)
		
		var stringComparisonOptions : String.CompareOptions {
			var out = String.CompareOptions()
			if self.contains(.caseInsensitive) {
				out.formUnion(.caseInsensitive)
			}
			if self.contains(.diacriticInsensitive) {
				out.formUnion(.diacriticInsensitive)
			}
			return out
		}
		
#if canImport(ObjectiveC)
		var predicateComparisionOptions : NSComparisonPredicate.Options {
			var out = NSComparisonPredicate.Options()
			if self.contains(.caseInsensitive) {
				out.formUnion(.caseInsensitive)
			}
			if self.contains(.diacriticInsensitive) {
				out.formUnion(.diacriticInsensitive)
			}
			return out
		}
#endif
	}
	
	case matches(regex: String, options: StringOperator.Options)
#if canImport(ObjectiveC)
	case like(pattern: String, options: StringOperator.Options)
#endif
	case hasPrefix(prefix: String, options: StringOperator.Options)
	case hasSuffix(suffix: String, options: StringOperator.Options)
	case contains(fragment: String, options: StringOperator.Options)
}

private func reduce(_ options: [StringOperator.Options]) -> StringOperator.Options {
	return options.reduce(StringOperator.Options()) { (a,b) in
		a.union(b)
	}
}

public func matches(_ regex: String, options: StringOperator.Options...) -> StringOperator {
	return .matches(regex: regex, options: reduce(options))
}

#if canImport(ObjectiveC)
public func like(_ pattern: String, options: StringOperator.Options...) -> StringOperator {
	return .like(pattern: pattern, options: reduce(options))
}
#endif

public func hasPrefix(_ prefix: String, options: StringOperator.Options...) -> StringOperator {
	return .hasPrefix(prefix: prefix, options: reduce(options))
}

public func hasSuffix(_ suffix: String, options: StringOperator.Options...) -> StringOperator {
	return .hasSuffix(suffix: suffix, options: reduce(options))
}

public func contains(_ fragment: String, options: StringOperator.Options...) -> StringOperator {
	return .contains(fragment: fragment, options: reduce(options))
}


