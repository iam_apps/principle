//
//  Composition.swift
//  Principle
//
//  Created by Alex Lynch on 9/8/20.
//  Copyright © 2020 IAM Apps. All rights reserved.
//

import Foundation

public func +<A,B,C>(lhs: @escaping (A)->B, rhs: @escaping (B)->C) -> (A)->C {
    { rhs(lhs($0)) }
}

public func +<A,B,C>(lhs: KeyPath<A,B>, rhs: @escaping (B)->C) -> (A)->C {
    { rhs($0[keyPath: lhs]) }
}

public func +<A,B,C>(lhs: @escaping (A)->B, rhs: KeyPath<B,C>) -> (A)->C {
    { lhs($0)[keyPath: rhs] }
}

