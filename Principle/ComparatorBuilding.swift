//
//  ComparatorBuilding.swift
//  Predikit
//
//  Created by Alex Lynch on 3/25/18.
//  Copyright © 2018 IAM Apps. All rights reserved.
//

import Foundation

public typealias Comparator<T> = (T, T) -> Bool

private func < <T: Comparable>(a: T?, b: T?) -> Bool {
	return compareOptionals(a: a, b: b, nilLast: true)
}

private func > <T: Comparable>(a: T?, b: T?) -> Bool {
	return compareOptionals(a: b, b: a, nilLast: false)
}

private func compareOptionals<T: Comparable>(a: T?, b: T?, nilLast: Bool = true) -> Bool {
	switch (a, b) {
	case let (realA?, realB?):	return realA < realB
	case (_?, nil):				return nilLast
	case (nil, _?), (nil, nil): return !nilLast
	}
}

public func | <Root,Value>(lhs: KeyPath<Root,Value>, rhs: GeneralComparison) -> Comparator<Root>  where Value : OptionalProtocol, Value.WrappedType : Comparable {
	switch rhs {
	case .asc:
		return { (a, b) in
			return a[keyPath: lhs].wrapped < b[keyPath: lhs].wrapped
		}
	case .desc:
		return { (a, b) in
			return a[keyPath: lhs].wrapped > b[keyPath: lhs].wrapped
		}
	}
}

public func | <Root,Value>(lhs: KeyPath<Root,Value>, rhs: GeneralComparison) -> Comparator<Root>  where Value : Comparable {
	switch rhs {
	case .asc:
		return { (a, b) in
			return a[keyPath: lhs] < b[keyPath: lhs]
		}
	case .desc:
		return { (a, b) in
			return a[keyPath: lhs] > b[keyPath: lhs]
		}
	}
}

public func | <Root,Value>(lhs: KeyPath<Root,Value>, rhs: DateComparison) -> Comparator<Root>  where Value : OptionalProtocol, Value.WrappedType == Date {
	return lhs|rhs.genaralComparison
}

public func | <Root>(lhs: KeyPath<Root,Date>, rhs: DateComparison) -> Comparator<Root> {
	return lhs|rhs.genaralComparison
}

