//
//  PredicateBuilding.swift
//  Predikit
//
//  Created by Alex Lynch on 2/21/18.
//  Copyright © 2018 IAM Apps. All rights reserved.
//

import Foundation
#if canImport(ObjectiveC)

public class Predicate<S> : NSCompoundPredicate {
	fileprivate init(from predicate: NSPredicate) {
		super.init(type: .and, subpredicates: [predicate])
	}
	
	override public init(type: NSCompoundPredicate.LogicalType, subpredicates: [NSPredicate]) {
		super.init(type: type, subpredicates: subpredicates)
	}
		
	required public init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

public func ==<S, E: Equatable>(lhs: KeyPath<S,E>, rhs: E) -> Predicate<S> {
	let predicate = NSComparisonPredicate(
			leftExpression: NSExpression(forKeyPath: lhs),
			rightExpression: NSExpression(forConstantValue: rhs),
			modifier: .direct,
			type: .equalTo,
			options: [])
	
	return Predicate<S>(from: predicate)
}

public func ==<S, Raw: RawRepresentable>(lhs: KeyPath<S,Raw>, rhs: Raw) -> Predicate<S> where Raw.RawValue: Equatable {
	let predicate = NSComparisonPredicate(
		leftExpression: NSExpression(forKeyPath: lhs),
		rightExpression: NSExpression(forConstantValue: rhs.rawValue),
		modifier: .direct,
		type: .equalTo,
		options: [])
	
	return Predicate<S>(from: predicate)
}


public func ==<S, Raw: RawRepresentable>(lhs: KeyPath<S,Raw>, rhs: Raw) -> Predicate<S> where Raw.RawValue: Equatable, Raw: Equatable {
	let predicate = NSComparisonPredicate(
		leftExpression: NSExpression(forKeyPath: lhs),
		rightExpression: NSExpression(forConstantValue: rhs.rawValue),
		modifier: .direct,
		type: .equalTo,
		options: [])
	
	return Predicate<S>(from: predicate)
}

public func ==<S, E: Equatable>(lhs: KeyPath<S,E?>, rhs: E) -> Predicate<S> {
	let predicate = NSComparisonPredicate(
		leftExpression: NSExpression(forKeyPath: lhs),
		rightExpression: NSExpression(forConstantValue: rhs),
		modifier: .direct,
		type: .equalTo,
		options: [])
	
	return Predicate<S>(from: predicate)
}

public func ==<S, Raw: RawRepresentable>(lhs: KeyPath<S,Raw?>, rhs: Raw) -> Predicate<S> where Raw.RawValue: Equatable {
	let predicate = NSComparisonPredicate(
		leftExpression: NSExpression(forKeyPath: lhs),
		rightExpression: NSExpression(forConstantValue: rhs.rawValue),
		modifier: .direct,
		type: .equalTo,
		options: [])
	
	return Predicate<S>(from: predicate)
}

public func ==<S, Raw: RawRepresentable>(lhs: KeyPath<S,Raw?>, rhs: Raw) -> Predicate<S> where Raw.RawValue: Equatable, Raw : Equatable {
	let predicate = NSComparisonPredicate(
		leftExpression: NSExpression(forKeyPath: lhs),
		rightExpression: NSExpression(forConstantValue: rhs.rawValue),
		modifier: .direct,
		type: .equalTo,
		options: [])
	
	return Predicate<S>(from: predicate)
}

public func !=<S, E: Equatable>(lhs: KeyPath<S,E>, rhs: E) -> Predicate<S> {
	return !(lhs == rhs)
}

public func !=<S, Raw: RawRepresentable>(lhs: KeyPath<S,Raw>, rhs: Raw) -> Predicate<S> where Raw.RawValue: Equatable {
	return !(lhs == rhs)
}

public func !=<S, Raw: RawRepresentable>(lhs: KeyPath<S,Raw>, rhs: Raw) -> Predicate<S> where Raw.RawValue: Equatable, Raw: Equatable {
	return !(lhs == rhs)
}

public func !=<S, E: Equatable>(lhs: KeyPath<S,E?>, rhs: E) -> Predicate<S> {
	return !(lhs == rhs)
}

public func !=<S, Raw: RawRepresentable>(lhs: KeyPath<S,Raw?>, rhs: Raw) -> Predicate<S> where Raw.RawValue: Equatable {
	return !(lhs == rhs)
}

public func !=<S, Raw: RawRepresentable>(lhs: KeyPath<S,Raw?>, rhs: Raw) -> Predicate<S> where Raw.RawValue: Equatable, Raw: Equatable {
	return !(lhs == rhs)
}

public func ==<S, E: Equatable>(lhs: KeyPath<S, E>, rhs: E?) -> Predicate<S> {
    let predicate = NSComparisonPredicate(
        leftExpression: NSExpression(forKeyPath: lhs),
        rightExpression: NSExpression(forConstantValue: rhs),
        modifier: .direct,
        type: .equalTo,
        options: [])

    return Predicate<S>(type: .and, subpredicates: [predicate])
}

public func !=<S, E: Equatable>(lhs: KeyPath<S, E>, rhs: E?) -> Predicate<S> {
    !(lhs == rhs)
}

public func < <S,C: Comparable>(lhs: KeyPath<S,C>, rhs: C) -> Predicate<S> {
	let predicate = NSComparisonPredicate(
		leftExpression: NSExpression(forKeyPath: lhs),
		rightExpression: NSExpression(forConstantValue: rhs),
		modifier: .direct,
		type: .lessThan,
		options: [])
	
	return Predicate<S>(from: predicate)
}

public func > <S,C: Comparable>(lhs: KeyPath<S,C>, rhs: C) -> Predicate<S> {
	let predicate = NSComparisonPredicate(
		leftExpression: NSExpression(forKeyPath: lhs),
		rightExpression: NSExpression(forConstantValue: rhs),
		modifier: .direct,
		type: .greaterThan,
		options: [])
	
	return Predicate<S>(from: predicate)
}

public func <= <S,C: Comparable>(lhs: KeyPath<S,C>, rhs: C) -> Predicate<S> {
	let predicate = NSComparisonPredicate(
		leftExpression: NSExpression(forKeyPath: lhs),
		rightExpression: NSExpression(forConstantValue: rhs),
		modifier: .direct,
		type: .lessThanOrEqualTo,
		options: [])
	
	return Predicate<S>(from: predicate)
}

public func >= <S,C: Comparable>(lhs: KeyPath<S,C>, rhs: C) -> Predicate<S> {
	let predicate = NSComparisonPredicate(
		leftExpression: NSExpression(forKeyPath: lhs),
		rightExpression: NSExpression(forConstantValue: rhs),
		modifier: .direct,
		type: .greaterThanOrEqualTo,
		options: [])
	
	return Predicate<S>(from: predicate)
}

public func < <S,C: Comparable>(lhs: KeyPath<S,C?>, rhs: C) -> Predicate<S> {
	let predicate = NSComparisonPredicate(
		leftExpression: NSExpression(forKeyPath: lhs),
		rightExpression: NSExpression(forConstantValue: rhs),
		modifier: .direct,
		type: .lessThan,
		options: [])
	
	return Predicate<S>(from: predicate)
}

public func > <S,C: Comparable>(lhs: KeyPath<S,C?>, rhs: C) -> Predicate<S> {
	let predicate = NSComparisonPredicate(
		leftExpression: NSExpression(forKeyPath: lhs),
		rightExpression: NSExpression(forConstantValue: rhs),
		modifier: .direct,
		type: .greaterThan,
		options: [])
	
	return Predicate<S>(from: predicate)
}

public func <= <S,C: Comparable>(lhs: KeyPath<S,C?>, rhs: C) -> Predicate<S> {
	let predicate = NSComparisonPredicate(
		leftExpression: NSExpression(forKeyPath: lhs),
		rightExpression: NSExpression(forConstantValue: rhs),
		modifier: .direct,
		type: .lessThanOrEqualTo,
		options: [])
	
	return Predicate<S>(from: predicate)
}

public func >= <S,C: Comparable>(lhs: KeyPath<S,C?>, rhs: C) -> Predicate<S> {
	let predicate = NSComparisonPredicate(
		leftExpression: NSExpression(forKeyPath: lhs),
		rightExpression: NSExpression(forConstantValue: rhs),
		modifier: .direct,
		type: .greaterThanOrEqualTo,
		options: [])
	
	return Predicate<S>(from: predicate)
}

public prefix func !<S>(filter: Predicate<S>) -> Predicate<S> {
	return Predicate<S>(from: NSCompoundPredicate(notPredicateWithSubpredicate: filter))
}

public func &&<S>(lhs: Predicate<S>, rhs: Predicate<S>) -> Predicate<S> {
	return Predicate<S>(from: NSCompoundPredicate(andPredicateWithSubpredicates: [lhs, rhs]))
}

public func ||<S>(lhs: Predicate<S>, rhs: Predicate<S>) -> Predicate<S> {
	return Predicate<S>(from: NSCompoundPredicate(orPredicateWithSubpredicates: [lhs, rhs]))
}

public prefix func !<S>(keyPath: KeyPath<S,Bool>) -> Predicate<S> {
	return keyPath != true
}

public func &&<S>(lhs: Predicate<S>, rhs: KeyPath<S,Bool>) -> Predicate<S> {
	return lhs && (rhs == true)
}

public func ||<S>(lhs: Predicate<S>, rhs: KeyPath<S,Bool>) -> Predicate<S> {
	return lhs || (rhs == true)
}

public func &&<S>(lhs: KeyPath<S,Bool>, rhs: Predicate<S>) -> Predicate<S> {
	return (lhs == true) && rhs
}

public func ||<S>(lhs: KeyPath<S,Bool>, rhs: Predicate<S>) -> Predicate<S> {
	return (lhs == true) || rhs
}

public func &&<S>(lhs: KeyPath<S,Bool>, rhs: KeyPath<S,Bool>) -> Predicate<S> {
	return (lhs == true) && (rhs == true)
}

public func ||<S>(lhs: KeyPath<S,Bool>, rhs: KeyPath<S,Bool>) -> Predicate<S> {
	return (lhs == true) || (rhs == true)
}

public func |<S>(lhs: KeyPath<S,String>, rhs: StringOperator) -> Predicate<S> {
	let pred = predicate(from: NSExpression(forKeyPath: lhs), and: rhs)
	return Predicate<S>(from: pred)
}
public func |<S>(lhs: KeyPath<S,String?>, rhs: StringOperator) -> Predicate<S> {
	let pred = predicate(from: NSExpression(forKeyPath: lhs), and: rhs)
	return Predicate<S>(from: pred)
}

private func predicate(from expression: NSExpression, and operator: StringOperator) -> NSPredicate {
	let rightConstant: Any
	let type: NSComparisonPredicate.Operator
	let stringOptions: StringOperator.Options
	switch `operator` {
	case let .matches(regex, options):
		rightConstant = regex
		type = .matches
		stringOptions = options
	case let .like(pattern, options):
		rightConstant = pattern
		type = .like
		stringOptions = options
	case let .hasPrefix(prefix, options) :
		rightConstant = prefix
		type = .beginsWith
		stringOptions = options
	case let .hasSuffix(postFix, options) :
		rightConstant = postFix
		type = .endsWith
		stringOptions = options
	case let .contains(fragment, options) :
		rightConstant = fragment
		type = .contains
		stringOptions = options
	}
	
	return NSComparisonPredicate(
		leftExpression: expression,
		rightExpression: NSExpression(forConstantValue: rightConstant),
		modifier: .direct,
		type: type,
		options: stringOptions.predicateComparisionOptions)
}

private func prefix(expression: NSExpression, with identifier: String) -> NSExpression {
	precondition(expression.expressionType == .keyPath, "\(#function) is only possible with .keyPath expressions")
	return NSExpression(format: "$\(identifier).\(expression.keyPath)")
}

private func restructure(comparisonPredicate: NSComparisonPredicate, with identifier: String) -> NSComparisonPredicate {
	func restructured(_ expression: NSExpression) -> NSExpression {
		if expression.expressionType == .keyPath {
			return prefix(expression: expression, with: identifier)
		} else {
			return expression
		}
	}
	
	let left = restructured(comparisonPredicate.leftExpression)
	let right = restructured(comparisonPredicate.rightExpression)
	
	return NSComparisonPredicate(leftExpression: left,
								 rightExpression: right,
								 modifier: comparisonPredicate.comparisonPredicateModifier,
								 type: comparisonPredicate.predicateOperatorType,
								 options: comparisonPredicate.options)
}

private func restucture(predicate: NSPredicate, with identifier: String) -> NSPredicate {
	if let compound = predicate as? NSCompoundPredicate {
		return restructure(compoundPredicate: compound, with: identifier)
	} else if let compare = predicate as? NSComparisonPredicate {
		return restructure(comparisonPredicate: compare, with: identifier)
	} else {
		return predicate // should never happen
	}
}

private func restructure(compoundPredicate: NSCompoundPredicate, with identifier: String) -> NSCompoundPredicate {
	guard let subPredicates = compoundPredicate.subpredicates as? [NSPredicate] else {
		preconditionFailure("Unable to cast subpredicates to NSPredicate.\n\(compoundPredicate.subpredicates)")
	}
	let restructuredSubPredicates = subPredicates.map { restucture(predicate: $0, with: identifier) }
	
	return NSCompoundPredicate(type: compoundPredicate.compoundPredicateType, subpredicates: restructuredSubPredicates)
}

public func any<S,C: Sequence>(_ keyPath: KeyPath<S,C>, match predicate: Predicate<C.Element>) -> Predicate<S> {
	let collectionExpression = NSExpression(forKeyPath: keyPath)
	let randomIdentifier = "_\(arc4random())"
	let prefixedPredicate = restucture(predicate: predicate, with: randomIdentifier)
	let subquery = NSExpression(forSubquery: collectionExpression, usingIteratorVariable: randomIdentifier, predicate: prefixedPredicate)
	
	let countExpression = NSExpression(forFunction: "count:", arguments: [subquery])
	let zeroExpression = NSExpression(forConstantValue: 0)
	
	return Predicate(from: NSComparisonPredicate(leftExpression: countExpression,
												 rightExpression: zeroExpression,
												 modifier: .direct,
												 type: .greaterThan,
												 options: []))
}

public func all<S,C: Sequence>(_ keyPath: KeyPath<S,C>, match predicate: Predicate<C.Element>) -> Predicate<S> {
	let collectionExpression = NSExpression(forKeyPath: keyPath)
	let randomIdentifier = "_\(arc4random())"
	let prefixedPredicate = restucture(predicate: predicate, with: randomIdentifier)
	let subquery = NSExpression(forSubquery: collectionExpression, usingIteratorVariable: randomIdentifier, predicate: prefixedPredicate)
	
	let countExpression = NSExpression(forFunction: "count:", arguments: [subquery])
	let totalExpression = NSExpression(forFunction: "count:", arguments: [collectionExpression])
	let zeroExpression = NSExpression(forConstantValue: 0)
	
	let allPredicate = NSComparisonPredicate(leftExpression: countExpression,
												 rightExpression: totalExpression,
												 modifier: .direct,
												 type: .equalTo,
												 options: [])
	
	let somePredicate = NSComparisonPredicate(leftExpression: totalExpression,
											  rightExpression: zeroExpression,
											  modifier: .direct,
											  type: .greaterThan,
											  options: [])
	
	return Predicate(from: NSCompoundPredicate(andPredicateWithSubpredicates: [allPredicate, somePredicate]))
}

public func no<S,C: Sequence>(_ keyPath: KeyPath<S,C>, match predicate: Predicate<C.Element>) -> Predicate<S> {
	let collectionExpression = NSExpression(forKeyPath: keyPath)
	let randomIdentifier = "_\(arc4random())"
	let prefixedPredicate = restucture(predicate: predicate, with: randomIdentifier)
	let subquery = NSExpression(forSubquery: collectionExpression, usingIteratorVariable: randomIdentifier, predicate: prefixedPredicate)
	
	let countExpression = NSExpression(forFunction: "count:", arguments: [subquery])
	let zeroExpression = NSExpression(forConstantValue: 0)
	
	return Predicate(from: NSComparisonPredicate(leftExpression: countExpression,
												 rightExpression: zeroExpression,
												 modifier: .direct,
												 type: .equalTo,
												 options: []))
}

#endif
