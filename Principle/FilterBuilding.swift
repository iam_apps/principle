//
//  FilterBuilding.swift
//  Predikit
//
//  Created by Alex Lynch on 2/21/18.
//  Copyright © 2018 IAM Apps. All rights reserved.
//

import Foundation


public typealias Filter<T> = (T) -> Bool

public func ==<S, E: Equatable>(lhs: KeyPath<S,E>, rhs: E) -> Filter<S> {
	return {
		$0[keyPath: lhs] == rhs
	}
}

public func ==<S, E: Equatable>(lhs: KeyPath<S,E?>, rhs: E) -> Filter<S> {
	return {
		if let e = $0[keyPath: lhs] {
			return e == rhs
		} else {
			return false
		}
	}
}

public func !=<S, E: Equatable>(lhs: KeyPath<S,E>, rhs: E) -> Filter<S> {
	return !(lhs == rhs)
}

public func !=<S, E: Equatable>(lhs: KeyPath<S,E?>, rhs: E) -> Filter<S> {
	return !(lhs == rhs)
}


public func ==<S, E: Equatable>(lhs: KeyPath<S, E>, rhs: E?) -> Filter<S> {
    { s in
        s[keyPath: lhs] == rhs
    }
}

public func !=<S, E: Equatable>(lhs: KeyPath<S, E>, rhs: E?) -> Filter<S> {
    !(lhs == rhs)
}


public func < <S,C: Comparable>(lhs: KeyPath<S,C>, rhs: C) -> Filter<S> {
	return {
		$0[keyPath: lhs] < rhs
	}
}

public func > <S,C: Comparable>(lhs: KeyPath<S,C>, rhs: C) -> Filter<S> {
	return {
		$0[keyPath: lhs] > rhs
	}
}

public func <= <S,C: Comparable>(lhs: KeyPath<S,C>, rhs: C) -> Filter<S> {
	return {
		$0[keyPath: lhs] <= rhs
	}
}

public func >= <S,C: Comparable>(lhs: KeyPath<S,C>, rhs: C) -> Filter<S> {
	return {
		$0[keyPath: lhs] >= rhs
	}
}

public func < <S,C: Comparable>(lhs: KeyPath<S,C?>, rhs: C) -> Filter<S> {
	return {
		if let c = $0[keyPath: lhs] {
			return c < rhs
		} else {
			return false
		}
	}
}

public func > <S,C: Comparable>(lhs: KeyPath<S,C?>, rhs: C) -> Filter<S> {
	return {
		if let c = $0[keyPath: lhs] {
			return c > rhs
		} else {
			return false
		}
	}
}

public func <= <S,C: Comparable>(lhs: KeyPath<S,C?>, rhs: C) -> Filter<S> {
	return {
		if let c = $0[keyPath: lhs] {
			return c <= rhs
		} else {
			return false
		}
	}
}

public func >= <S,C: Comparable>(lhs: KeyPath<S,C?>, rhs: C) -> Filter<S> {
	return {
		if let c = $0[keyPath: lhs] {
			return c >= rhs
		} else {
			return false
		}
	}
}

public prefix func !<S>(filter: @escaping Filter<S>) -> Filter<S> {
	return {
		!filter($0)
	}
}

public func &&<S>(lhs: @escaping Filter<S>, rhs: @escaping Filter<S>) -> Filter<S> {
	return {
		lhs($0) && rhs($0)
	}
}

public func ||<S>(lhs: @escaping Filter<S>, rhs: @escaping Filter<S>) -> Filter<S> {
	return {
		lhs($0) || rhs($0)
	}
}

public prefix func !<S>(keyPath: KeyPath<S,Bool>) -> Filter<S> {
	return {
		!$0[keyPath: keyPath]
	}
}

public func &&<S>(lhs: @escaping Filter<S>, rhs: KeyPath<S,Bool>) -> Filter<S> {
	return {
		lhs($0) && $0[keyPath: rhs]
	}
}

public func ||<S>(lhs: @escaping Filter<S>, rhs: KeyPath<S,Bool>) -> Filter<S> {
	return {
		lhs($0) || $0[keyPath: rhs]
	}
}

public func &&<S>(lhs: KeyPath<S,Bool>, rhs: @escaping Filter<S>) -> Filter<S> {
	return {
		$0[keyPath: lhs] && rhs($0)
	}
}

public func ||<S>(lhs: KeyPath<S,Bool>, rhs: @escaping Filter<S>) -> Filter<S> {
	return {
		$0[keyPath: lhs] || rhs($0)
	}
}

public func &&<S>(lhs: KeyPath<S,Bool>, rhs: KeyPath<S,Bool>) -> Filter<S> {
	return {
		$0[keyPath: lhs] && $0[keyPath: lhs]
	}
}

public func ||<S>(lhs: KeyPath<S,Bool>, rhs: KeyPath<S,Bool>) -> Filter<S> {
	return {
		$0[keyPath: lhs] || $0[keyPath: lhs]
	}
}

public func |<S>(lhs: KeyPath<S,String>, rhs: StringOperator) -> Filter<S> {
	return {
		filter(from: rhs)($0[keyPath: lhs])
	}
	
}
public func |<S>(lhs: KeyPath<S,String?>, rhs: StringOperator) -> Filter<S> {
	return {
		if let string = $0[keyPath: lhs] {
			return filter(from: rhs)(string)
		} else {
			return false
		}
	}
}

private func filter(from operator: StringOperator) -> Filter<String> {
	switch `operator` {
	case let .matches(regex, options):
		let finalOptions = options.stringComparisonOptions.union(.regularExpression)
		return {
			($0 as NSString).range(of: regex, options: finalOptions).location != NSNotFound
		}
#if canImport(ObjectiveC)
	case let .like(pattern, options):
		let predicate = NSComparisonPredicate(
			leftExpression: NSExpression.expressionForEvaluatedObject(),
			rightExpression: NSExpression(forConstantValue: pattern),
			modifier: .direct,
			type: .like,
			options: options.predicateComparisionOptions)
		return {
			predicate.evaluate(with: $0)
		}
#endif
	case let .hasPrefix(prefix, options) :
		if options == [] {
			return {
				$0.hasPrefix(prefix)
			}
		} else {
			let finalOptions = options.stringComparisonOptions
			return {
				($0 as NSString).range(of: prefix, options: finalOptions).location == 0
			}
		}
	case let .hasSuffix(suffix, options) :
		if options == [] {
			return {
				$0.hasSuffix(suffix)
			}
		} else {
			let finalOptions = options.stringComparisonOptions
			return {
				let matchingLocation = $0.count - suffix.count
				return ($0 as NSString).range(of: suffix, options: finalOptions).location == matchingLocation
			}
		}
	case let .contains(fragment, options) :
		let finalOptions = options.stringComparisonOptions
		return {
			($0 as NSString).range(of: fragment, options: finalOptions).location != NSNotFound
		}
	}
}

public func any<S,C: Sequence>(_ keyPath: KeyPath<S,C>, match filter: @escaping Filter<C.Element>) -> Filter<S> {
	return {
		let sequence = $0[keyPath: keyPath]
		return sequence.first(where: filter) != nil
	}
}

public func all<S,C: Sequence>(_ keyPath: KeyPath<S,C>, match filter: @escaping Filter<C.Element>) -> Filter<S> {
	return {
		let sequence = $0[keyPath: keyPath]
		var hasElements = false
		var isPassing = true
		for element in sequence {
			hasElements = true
			isPassing = filter(element)
			if !isPassing { break }
		}
		
		return hasElements && isPassing
	}
}

public func no<S,C: Sequence>(_ keyPath: KeyPath<S,C>, match filter: @escaping Filter<C.Element>) -> Filter<S> {
	return {
		let sequence = $0[keyPath: keyPath]
		return sequence.first(where: filter) == nil
	}
}

