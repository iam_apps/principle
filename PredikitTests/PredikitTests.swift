//
//  PredikitTests.swift
//  PredikitTests
//
//  Created by Alex Lynch on 2/21/18.
//  Copyright © 2018 IAM Apps. All rights reserved.
//

import XCTest
import Predikit

class PredikitTests: XCTestCase {
	
	class Person : NSObject {
		@objc
		let name: String?
		@objc
		let age: Int
		
		init(name: String, age: Int) {
			self.name = name
			self.age = age
		}
	}
	
	let jerry = Person(name: "Jerry", age: 50)
	let michael = Person(name: "Michael", age: 40)
	let bob = Person(name: "Bob", age: 30)
	
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFilterBuliding() {
		let normal  = { (s: String) in return s.count > 4 || s.isEmpty }
		let special:Filter<String> = \String.count > 4 || \.isEmpty
		
		
		XCTAssert(special("cat") == normal("cat"))
		XCTAssert(special("Hello") == normal("Hello"))
		XCTAssert(special("") == normal(""))
    }
    
    func testPredicateBuliding() {
		
		let predicate1: Predicate<Person> = \Person.name == "Jerry" || \.age <= 30
		
		XCTAssert(predicate1.evaluate(with: jerry) == true)
		XCTAssert(predicate1.evaluate(with: michael) == false)
		XCTAssert(predicate1.evaluate(with: bob) == true)
    }
	
	
	
	func testComparatorBuilding() {
		let data = [jerry, michael, bob]
		
		let ageData = data.sorted(by: \.age|asc)
		XCTAssert(ageData == [bob, michael, jerry])
		
		let nameData = data.sorted(by: \.name|desc)
		XCTAssert(nameData == [michael, jerry, bob])
	}
	
	func testSortDescriptorBuilding() {
		let data:NSArray = [jerry, michael, bob]
		
		let sorted = data.sortedArray(using: [\Person.age|asc])
		let ageData = sorted as! Array<Person>
		XCTAssert(ageData == [bob, michael, jerry])
		
		let nameData = data.sortedArray(using: [\Person.name|desc]) as! Array<Person>
		XCTAssert(nameData == [michael, jerry, bob])
	}
    
}
