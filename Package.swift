// swift-tools-version:5.2
import PackageDescription
let package = Package(
    name: "Principle",
    products: [
        .library(
            name: "Principle",
            targets: ["Principle"]),
    ],
    dependencies: [],
    targets: [
        .target(
            name: "Principle",
            dependencies: [],
            path: "Principle"),
        .testTarget(
            name: "PrincipleTests",
            dependencies: ["Principle"],
            path: "PrincipleTests"),
    ]
)