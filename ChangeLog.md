
#### [Current]

#### 
 * [2d757b8](../../commit/2d757b8) - __(Alex Lynch)__ Introduce function composition operator.
 * [e7fb861](../../commit/e7fb861) - __(Alex Lynch)__ Mark as v1.2
 * [a815639](../../commit/a815639) - __(Alex Lynch)__ Mark as v1.1
 * [6bf8684](../../commit/6bf8684) - __(Alex Lynch)__ Support RawRepresentable values when building Predicates. Implement missing != operators.
 * [0c97b97](../../commit/0c97b97) - __(Alex Lynch)__ Mark as v1.0.1
 * [30c78d6](../../commit/30c78d6) - __(Alex Lynch)__ Update readme.
 * [d84b1e9](../../commit/d84b1e9) - __(Alex Lynch)__ Mark as v1.0
 * [58cc6f7](../../commit/58cc6f7) - __(Alex Lynch)__ Implement subqueries.
 * [43eb85f](../../commit/43eb85f) - __(Alex Lynch)__ Update make_release script.
 * [3648200](../../commit/3648200) - __(Alex Lynch)__ Mark as v0.10
 * [bdca27e](../../commit/bdca27e) - __(Alex Lynch)__ Mark as v0.10
 * [ec3fc31](../../commit/ec3fc31) - __(Alex Lynch)__ Mark as v0.10
 * [821d8ce](../../commit/821d8ce) - __(Alex Lynch)__ Implement optional comparable filters.
 * [756344e](../../commit/756344e) - __(Alex Lynch)__ Updates for swift 4.1
 * [efdd259](../../commit/efdd259) - __(Alex Lynch)__ Mark as v0.9
 * [1233d34](../../commit/1233d34) - __(Alex Lynch)__ Implement string comparison operators.
 * [8e1a1cb](../../commit/8e1a1cb) - __(Alex Lynch)__ Mark as v0.8.4
 * [2d33e09](../../commit/2d33e09) - __(Alex Lynch)__ Mark as v0.8.4
 * [27cead5](../../commit/27cead5) - __(Alex Lynch)__ Mark as v0.8.4
 * [c44faa5](../../commit/c44faa5) - __(Alex Lynch)__ Mark as v0.8.4
 * [6086796](../../commit/6086796) - __(Alex Lynch)__ Mark as v0.8.4
 * [29626cc](../../commit/29626cc) - __(Alex Lynch)__ Mark as v0.8.4
 * [73f6a14](../../commit/73f6a14) - __(Alex Lynch)__ First pass at a readme.
 * [6adad9b](../../commit/6adad9b) - __(Alex Lynch)__ Mark as v0.8.3
 * [d031758](../../commit/d031758) - __(Alex Lynch)__ Mark as v0.8.3
 * [fe5a540](../../commit/fe5a540) - __(Alex Lynch)__ Rename project.
 * [b3d288f](../../commit/b3d288f) - __(Alex Lynch)__ Mark as v0.8.2
 * [0ec72c8](../../commit/0ec72c8) - __(Alex Lynch)__ Add license. Remove spurious UIKit import.
 * [783c983](../../commit/783c983) - __(Alex Lynch)__ Mark as v0.8.1
 * [26244c4](../../commit/26244c4) - __(Alex Lynch)__ Updates to release file.
 * [a7617b4](../../commit/a7617b4) - __(Alex Lynch)__ Mark as v0.8
 * [69d27fa](../../commit/69d27fa) - __(Alex Lynch)__ Mark as v0.8
 * [ad4e80d](../../commit/ad4e80d) - __(Alex Lynch)__ Mark as v0.8
 * [23c5321](../../commit/23c5321) - __(Alex Lynch)__ Restore cleaner SortDescriptor constructor, preserving CoreData compatible semantics.
 * [960870c](../../commit/960870c) - __(Alex Lynch)__ Revise NSSortDescriptor construction. Confirmed to work with CoreData.
 * [7fd035c](../../commit/7fd035c) - __(Alex Lynch)__ Correct faults in FilterBuilding and PredicateBuilding. Shown to works with CoreData.
 * [046e728](../../commit/046e728) - __(Alex Lynch)__ Proof of concept.
 * [6aa0cf1](../../commit/6aa0cf1) - __(Alex Lynch)__ Initial Commit
