
Pod::Spec.new do |s|
  s.name         = "Principle"
  s.version      = "1.3"
  s.summary      = "A swift library to create compile-time checked NSPredicates, NSSortDesciptions, Filters and Comparators"

  s.homepage     = "https://bitbucket.org/iam_apps/principle/src/master/"
	s.documentation_url = s.homepage

  s.license      = { :type=>"MIT" }

  s.author       = { "Alex Lynch" => "alex@iamapps.net" }

  s.ios.deployment_target = "10.0"
  s.osx.deployment_target = "10.11"
  s.tvos.deployment_target = "10.0"
  s.pod_target_xcconfig = { "SWIFT_VERSION" => "4.1" }

  s.source       = { :git => "https://bitbucket.org/iam_apps/principle.git", :tag=>"v1.3"}

  s.source_files = "Principle/*.swift"
end	
