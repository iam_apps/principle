//
//  PrincipleTests.swift
//  PrincipleTests
//
//  Created by Alex Lynch on 3/25/18.
//  Copyright © 2018 IAM Apps. All rights reserved.
//

import XCTest
import Principle

@objcMembers
class Person : NSObject {
	let id: String
	let name: String?
	let age: Int
	
	var pets = Set<Pet>()
	
	init(id: String, name: String, age: Int) {
		self.id = id
		self.name = name
		self.age = age
	}
	
	override var description: String {
		return self.name ?? "No-name Person"
	}
}

@objcMembers
class Pet: NSObject {
	let name: String?
	let legs: Int
	
	init(name: String?, legs: Int) {
		self.name = name
		self.legs = legs
	}
	
	override var description: String {
		return self.name ?? "No-name Pet"
	}
}

class PrincipleTests: XCTestCase {
	
	let jerry = Person(id: "11", name: "Jerry", age: 50)
	let michael = Person(id: "12", name: "Michael", age: 40)
	let bob = Person(id: "13", name: "Bob", age: 30)
	
	override func setUp() {
		super.setUp()
		
		jerry.pets = [Pet(name: "Spot", legs: 4), Pet(name: "Lucky", legs: 3)]
		michael.pets = [Pet(name: "Clive", legs: 2), Pet(name: nil, legs: 8)]
	}
	
	override func tearDown() {
		// Put teardown code here. This method is called after the invocation of each test method in the class.
		super.tearDown()
	}
	
	func testFilterBuliding() {
		let normal  = { (s: String) in return s.count > 4 || s.isEmpty }
		let special:Filter<String> = \String.count > 4 || \.isEmpty
		
		
		XCTAssert(special("cat") == normal("cat"))
		XCTAssert(special("Hello") == normal("Hello"))
		XCTAssert(special("") == normal(""))
	}
	
	func testPredicateBuliding() {
		
		let predicate1: Predicate<Person> = \Person.name == "Jerry" || \.age <= 30
		
		XCTAssert(predicate1.evaluate(with: jerry) == true)
		XCTAssert(predicate1.evaluate(with: michael) == false)
		XCTAssert(predicate1.evaluate(with: bob) == true)
	}
	
	func testCompountFilter() {
		let data = [jerry, michael, bob]
		
		let filtered1 = data.filter(\.age <= 35 || \.name|contains("rr"))
		XCTAssertEqual(filtered1, [jerry,bob])
	}
	
	func testComparatorBuilding() {
		let data = [jerry, michael, bob]
		
		let ageData = data.sorted(by: \.age|asc)
		XCTAssert(ageData == [bob, michael, jerry])
		
		let nameData = data.sorted(by: \.name|desc)
		XCTAssert(nameData == [michael, jerry, bob])
	}
	
	func testSortDescriptorBuilding() {
		let data:NSArray = [jerry, michael, bob]
		
		let sorted = data.sortedArray(using: [\Person.age|asc])
		let ageData = sorted as! Array<Person>
		XCTAssert(ageData == [bob, michael, jerry])
		
		let nameData = data.sortedArray(using: [\Person.name|desc]) as! Array<Person>
		XCTAssert(nameData == [michael, jerry, bob])
	}
	
	func testPredicateLikeOperator() {
		let predicate1:NSPredicate = \Person.name|like("*err*")
		let predicate2:NSPredicate = \Person.id|like("*3")
		
		let data:NSArray = [jerry, michael, bob]
		
		let filterd1 = data.filtered(using: predicate1) as! [Person]
		let filterd2 = data.filtered(using: predicate2) as! [Person]
		
		XCTAssertEqual(filterd1, [jerry])
		XCTAssertEqual(filterd2, [bob])
	}
	
	func testPredicateMatchesOperator() {
		let predicate1:NSPredicate = \Person.name|matches(".*r{2}.*")
		let predicate2:NSPredicate = \Person.id|matches(".3")
		
		let data:NSArray = [jerry, michael, bob]
		
		let filterd1 = data.filtered(using: predicate1) as! [Person]
		let filterd2 = data.filtered(using: predicate2) as! [Person]
		
		XCTAssertEqual(filterd1, [jerry])
		XCTAssertEqual(filterd2, [bob])
	}
	
	func testPredicatePrefixOperator() {
		let predicate1:NSPredicate = \Person.name|hasPrefix("Jer")
		let predicate2:NSPredicate = \Person.id|hasPrefix("1")
		
		let data:NSArray = [jerry, michael, bob]
		
		let filterd1 = data.filtered(using: predicate1) as! [Person]
		let filterd2 = data.filtered(using: predicate2) as! [Person]
		
		XCTAssertEqual(filterd1, [jerry])
		XCTAssertEqual(filterd2,  [jerry, michael, bob])
	}
	
	func testPredicateProstfixOperator() {
		let predicate1:NSPredicate = \Person.name|hasSuffix("rry")
		let predicate2:NSPredicate = \Person.id|hasSuffix("2")
		
		let data:NSArray = [jerry, michael, bob]
		
		let filterd1 = data.filtered(using: predicate1) as! [Person]
		let filterd2 = data.filtered(using: predicate2) as! [Person]
		
		XCTAssertEqual(filterd1, [jerry])
		XCTAssertEqual(filterd2,  [michael])
	}
	
	func testPredicateContainsOperator() {
		let predicate1:NSPredicate = \Person.name|contains("rr")
		let predicate2:NSPredicate = \Person.id|contains("2")
		
		let data:NSArray = [jerry, michael, bob]
		
		let filterd1 = data.filtered(using: predicate1) as! [Person]
		let filterd2 = data.filtered(using: predicate2) as! [Person]
		
		XCTAssertEqual(filterd1, [jerry])
		XCTAssertEqual(filterd2,  [michael])
	}
	
	func testPredicatePrefixWithOptionsOperator() {
		let predicate1:NSPredicate = \Person.name|hasPrefix("jer", options: .caseInsensitive)
		let predicate2:NSPredicate = \Person.name|hasPrefix("jer")
		
		let data:NSArray = [jerry, michael, bob]
		
		let filterd1 = data.filtered(using: predicate1) as! [Person]
		let filterd2 = data.filtered(using: predicate2) as! [Person]
		
		XCTAssertEqual(filterd1, [jerry])
		XCTAssertEqual(filterd2,  [])
	}
	
	func testFilterLikeOperator() {
		let predicate1:Filter<Person> = \Person.name|like("*err*")
		let predicate2:Filter<Person> = \Person.id|like("*3")
		
		let data = [jerry, michael, bob]
		
		let filterd1 = data.filter(predicate1)
		let filterd2 = data.filter(predicate2)
		
		XCTAssertEqual(filterd1, [jerry])
		XCTAssertEqual(filterd2, [bob])
	}
	
	func testFilterMatchesOperator() {
		let predicate1:Filter<Person> = \Person.name|matches(".*r{2}.*")
		let predicate2:Filter<Person> = \Person.id|matches(".3")
		
		let data = [jerry, michael, bob]
		
		let filterd1 = data.filter(predicate1)
		let filterd2 = data.filter(predicate2)
		
		XCTAssertEqual(filterd1, [jerry])
		XCTAssertEqual(filterd2, [bob])
	}
	
	func testFilterPrefixOperator() {
		let predicate1:Filter<Person> = \Person.name|hasPrefix("Jer")
		let predicate2:Filter<Person> = \Person.id|hasPrefix("1")
		
		let data = [jerry, michael, bob]
		
		let filterd1 = data.filter(predicate1)
		let filterd2 = data.filter(predicate2)
		
		XCTAssertEqual(filterd1, [jerry])
		XCTAssertEqual(filterd2,  [jerry, michael, bob])
	}
	
	func testFilterProstfixOperator() {
		let predicate1:Filter<Person> = \Person.name|hasSuffix("rry")
		let predicate2:Filter<Person> = \Person.id|hasSuffix("2")
		
		let data = [jerry, michael, bob]
		
		let filterd1 = data.filter(predicate1)
		let filterd2 = data.filter(predicate2)
		
		XCTAssertEqual(filterd1, [jerry])
		XCTAssertEqual(filterd2,  [michael])
	}
	
	func testFilterContainsOperator() {
		let predicate1:Filter<Person> = \Person.name|contains("rr")
		let predicate2:Filter<Person> = \Person.id|contains("2")
		
		let data = [jerry, michael, bob]
		
		let filterd1 = data.filter(predicate1)
		let filterd2 = data.filter(predicate2)
		
		XCTAssertEqual(filterd1, [jerry])
		XCTAssertEqual(filterd2,  [michael])
	}
	
	func testFilterPrefixWithOptionsOperator() {
		let predicate1:Filter<Person> = \Person.name|hasPrefix("jer", options: .caseInsensitive)
		let predicate2:Filter<Person> = \Person.name|hasPrefix("jer")
		
		let data = [jerry, michael, bob]
		
		let filterd1 = data.filter(predicate1)
		let filterd2 = data.filter(predicate2)
		
		XCTAssertEqual(filterd1, [jerry])
		XCTAssertEqual(filterd2,  [])
	}
	
	func testOptionalComparableFiltering() {
		let data = [jerry, michael, bob]
		
		let filtered1 = data.filter(\.id.count > 0)
		let filtered2 = data.filter(\.name?.count > 3)
		
		XCTAssertEqual(filtered1, [jerry, michael, bob])
		XCTAssertEqual(filtered2, [jerry, michael])
	}
	
	func testOptionalComparablePredicate() {
		let data:NSArray = [jerry, michael, bob]
		
		if data.count > 1000 { // †
			let predicate1:NSPredicate = \Person.id.count > 0
			let predicate2:NSPredicate = \Person.name?.count > 3
			
			let filterd1 = data.filtered(using: predicate1) as! [Person]
			let filterd2 = data.filtered(using: predicate2) as! [Person]
			
			XCTAssertEqual(filterd1, [jerry])
			XCTAssertEqual(filterd2, [jerry, michael])
		}
		
		// † This code compiles but will crash at runtime because String.count is not (and cannot be) @objc.
		// This "test" exists just to ensure that the optional-comparable sematics compile.
	}
	
	func testFilterSubqueries() {
		let data = [jerry, michael, bob]
		
		let filtered1 = data.filter(\.id.count > 0 && any(\.pets, match: \.legs > 4))
		let filtered2 = data.filter(all(\.pets, match: \.legs >= 2))
		let filtered3 = data.filter(no(\.pets, match: \.legs >= 2))
		
		XCTAssertEqual(filtered1, [michael])
		XCTAssertEqual(filtered2, [jerry, michael])
		XCTAssertEqual(filtered3, [bob])
	}
	
	func testPredicateSubqueries() {
		let predicate1:Predicate<Person> = any(\.pets, match: \.legs > 4 && \.name != "jack")
		let predicate2:NSPredicate = all(\Person.pets, match: \.legs >= 2)
		let predicate3:NSPredicate = no(\Person.pets, match: \.legs >= 2)
		
		let data:NSArray = [jerry, michael, bob]
		
		let filterd1 = data.filtered(using: predicate1) as! [Person]
		let filterd2 = data.filtered(using: predicate2) as! [Person]
		let filterd3 = data.filtered(using: predicate3) as! [Person]
		
		XCTAssertEqual(filterd1, [michael])
		XCTAssertEqual(filterd2, [jerry, michael])
		XCTAssertEqual(filterd3, [bob])
	}
}
